angular-gulp-seed
=====================

## Dependencies

```bash
$ npm install -g gulp bower
$ npm install
$ bower install
```

## Builds

```bash
$ gulp build
$ gulp watch
$ gulp release
```