angular.module('app.config', [])

.config(function($urlRouterProvider, $stateProvider) {

  $urlRouterProvider.when('','/home');
  
  $stateProvider
  .state('app', {
    abstract: true,
    templateUrl: 'app/views/app.tpl.html',
    controller: 'app.controller'
  });
})

.run(function($rootScope) {
  
});