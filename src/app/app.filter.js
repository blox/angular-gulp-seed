angular.module('app.filter',[])

.filter('fromJson', function () {
  return function (string) {
    if(string){
      return JSON.parse(string);
    }
  };
})

.filter('isEmpty', function () {
  var bar;
  return function (obj) {
    for (bar in obj) {
      if (obj.hasOwnProperty(bar)) {
        return false;
      }
    }
    return true;
  };
})

.filter('toArray', function () {
  return function (obj, addKey) {
    if (!obj) return obj;
    if ( addKey === false ) {
      return Object.keys(obj).map(function(key) {
        return obj[key];
      });
    } else {
      return Object.keys(obj).map(function (key) {
        return Object.defineProperty(obj[key], '$key', { enumerable: false, value: key});
      });
    }
  };
});