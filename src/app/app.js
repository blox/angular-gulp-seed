var app = angular.module('app', [
  'ui.router',
  'app.config',
  'app.controller',
  'app.templates',
  'app.service',
  'app.filter',
  'app.directive',
  'home'
]);