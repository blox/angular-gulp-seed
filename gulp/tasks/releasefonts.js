var gulp = require('gulp');
var config = require('../config').releasefonts;

gulp.task('releasefonts', ['copyfonts', 'copyvendorfonts'], function() {
  return gulp.src(config.src)
    .pipe(gulp.dest(config.dest));
});
