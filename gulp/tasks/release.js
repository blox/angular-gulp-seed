var gulp = require('gulp');

gulp.task('release', [
  'expect',
  'releasejson',
  'releaseimages',
  'releasefonts',
  'releaseindex'
]);