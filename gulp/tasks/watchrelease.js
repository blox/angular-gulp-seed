var gulp  = require('gulp');
var config = require('../config').watch;

gulp.task('watchrelease', ['browsersyncrelease'], function() {
  gulp.watch(config.sass, ['releasecssheader']);
  gulp.watch(config.index, ['releaseindex']);
  gulp.watch(config.templates, ['releaseheader']);
  gulp.watch(config.javascript, ['releaseheader']);
});