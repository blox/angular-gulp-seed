var gulp = require('gulp');
var flatten = require('gulp-flatten');
var config = require('../config').releaseimages;

gulp.task('releaseimages', ['copyimages'], function() {
  return gulp.src(config.src)
    .pipe(flatten())
    .pipe(gulp.dest(config.dest));
});
