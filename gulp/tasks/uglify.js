var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var config = require('../config').uglify;
var ngAnnotate = require('gulp-ng-annotate');

gulp.task('uglify', ['templates', 'copyjs', 'copyvendorjs'], function() {
  return gulp.src(config.src)
    .pipe(ngAnnotate())
    .pipe(concat(config.concat))
    .pipe(uglify(config.options))
    .pipe(gulp.dest(config.dest))
});
