var gulp = require('gulp');

gulp.task('build', [
  'expect',
  'copyjson',
  'copyimages',
  'copyfonts',
  'copyvendorfonts',
  'index'
]);