var gulp = require('gulp');
var flatten = require('gulp-flatten');
var config = require('../config').copyjson;

gulp.task('copyjson', function() {
  return gulp.src(config.src)
    .pipe(flatten())
    .pipe(gulp.dest(config.dest));
});
