var gulp  = require('gulp');
var config = require('../config').watch;

gulp.task('watch', ['browsersync'], function() {
  gulp.watch(config.sass, ['sass']);
  gulp.watch(config.index, ['index']);
  gulp.watch(config.templates, ['templates']);
  gulp.watch(config.javascript, ['copyjs']);
});