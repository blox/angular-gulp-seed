var gulp = require('gulp');
var inject = require("gulp-inject");
var config = require('../config').index;                                                         

gulp.task('index', ['copyindex', 'templates', 'copyjs', 'copyvendorjs', 'sass'], function () {
  
  var target = gulp.src(config.src);
  var sources = gulp.src(config.sources, {read: false});

  return target.pipe(inject(sources, config.options))
    .pipe(gulp.dest(config.dest));
});