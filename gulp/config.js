var modRewrite = require('connect-modrewrite');
var compress = require('compression');
var dest = "./www";
var src = './src';
var vendor = "./bower_components";
var npms = "./node_modules";
var release = "./release";

module.exports = {
  aws:{
    src: release + '/**/*'
  },
  releasejsheader:{
    src:[
      release+'/js/app.js'
    ],
    dest:release+'/js'
  },
  releasecssheader:{
    src:[
      release+'/css/app.css'
    ],
    dest:release+'/css'
  },
  releaseimages:{
    src: dest + "/img/*",
    dest: release + "/img"     
  },
  releasejson:{
    src: dest + "/json/*",
    dest: release + "/json"     
  },
  uglify:{
    src:[
      dest+'/css/*.css',
      dest+'/js/angular.min.js',
      dest+'/js/angular-ui-router.min.js',
      dest+'/js/!(app.js|bootstrap.js)',
      dest+'/js/*.js',
      dest+'/js/app.js',
    ],
    dest:release+'/js',
    concat:'app.js',
    options:{
      mangle:true
    }
  },
  releasecss:{
    src: dest + "/css/app.css",
    dest: release + "/css"    
  },
  releasefonts:{
    src:dest+'/fonts/*.*',
    dest:release+'/fonts'
  },
  releaseindex:{
    src: release + "/index.html",
    dest: release,
    sources:[
      release+'/css/app.css',
      release+'/js/app.js',
    ],
    options:{
      ignorePath:['/release'],
      addRootSlash:true
    }    
  },
  releasecopyindex: {
    src: src + "/app/views/index.html",
    dest: release
  },
  releasecleanindex:{
    src: release + '/index.html'
  },
  expect:{
    paths:[
      {
        location:npms,
        message:"You must download npm dependencies using:",
        fix:"npm install"
      },
      {
        location:vendor,
        message:"You must download bower dependencies using:",
        fix:"bower install"
      }
    ]
  },
  browsersync: {
    ghostMode: false,
    server: {
      baseDir: [dest, src],
      middleware: [
        modRewrite([
          '!\.html|\.js|\.swf|\.webm|\.mp4|\.css|\.jpg|\.jpeg|\.gif|\.png|\.eot|\.svg|\.ttf|\.woff$ /index.html [L]'
        ])
      ]
    },
    files: [
      dest + "/**",
      "!" + dest + "/**.map"
    ]
  },
  browsersyncrelease: {
    ghostMode: false,
    server: {
      baseDir: [release, src],
      middleware: [
        modRewrite([
          '!\.html|\.js|\.swf|\.webm|\.mp4|\.css|\.jpg|\.jpeg|\.gif|\.png|\.eot|\.svg|\.ttf|\.woff$ /index.html [L]'
        ]),
        function(req,res,next){
          var gzip = compress();
          gzip(req,res,next);
        }
      ]
    },
    files: [
      release + "/**",
      "!" + release + "/**.map"
    ]
  },
  index: {  
    src: dest + "/index.html",
    dest: dest,
    sources:[
      dest+'/css/*.css',
      dest+'/js/angular.min.js',
      dest+'/js/angular-ui-router.min.js',
      dest+'/js/!(app.js|bootstrap.js)',
      dest+'/js/*.js',
      dest+'/js/app.js',
    ],
    options:{
      ignorePath:['/www'],
      addRootSlash:false
    }
  },
  cleanindex:{
    src: dest + '/index.html'
  },
  copywebcomponents: {
    src: [
    ],
    dest: dest + "/components"        
  },
  copyvendorjs: {
    src: [
      vendor+"/angular-ui-router/release/angular-ui-router.min.js",
      vendor+"/angular/angular.min.js",
    ],
    dest: dest + "/js"        
  },
  copyvendorfonts: {
    src: [],
    dest: dest + "/fonts"        
  },
  copyjs: {
    src: src + "/**/*.js",
    dest: dest + "/js"    
  },
  copyindex: {
    src: src + "/app/views/index.html",
    dest: dest
  },
  copyimages: {
    src: src + "/app/img/*.{jpg,jpeg,gif,png,svg,webm,mp4}",
    dest: dest + "/img"
  },
  copyjson: {
    src: src + "/**/*.json",
    dest: dest + "/json"
  },
  copyfonts: {
    src: src + "/app/fonts/*.*",
    dest: dest + "/fonts"
  },
  sass: {
    src: src + "/app/scss/app.scss",
    dest: dest + "/css"
  },
  watch: {
    index:src + "/app/views/index.html",
    templates:src + "/**/*.tpl.html",
    javascript:[src + "/**/*.js",src + "/**/*.json"],
    sass:src + "/**/*.scss",
    fonts:src + "/**/*.{eot,svg,ttf,woff}",
    images:src + "/**/*.{jpg,jpeg,gif,png,svg}",
    json: src + "/**/*.json",
  },
  templates:{
    src: src + "/**/*.tpl.html",
    dest: dest + "/js",
    filename: 'app.templates.js',
    options:{
      root:'',
      module:'app.templates',
      base:'',
      standalone:true
    }
  }
};
